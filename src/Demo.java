import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Demo {
    public static void main(String[] args) {
        regexExample5();
    }

    static void assertInPool() {
        String nameFirst = "Bohdan";
        String nameSecond = "Bohdan";

        assert nameFirst == nameSecond; // true
    }

    static void assertOutOfPool() {
        String nameFirst = new String("Bohdan");
        String nameSecond = new String("Bohdan");

        assert nameFirst == nameSecond; // false
    }

    static void stringForEach() {
        String name = "Bohdan";

        for (char ch : name.toCharArray()) {
            System.out.println(ch);
        }
    }

    static void compateToExample() {
        String name = "bohdan";
        System.out.println(name.compareTo("bohdaq"));
    }

    static void regexExample1() {
        Pattern pattern = Pattern.compile("(\\D)+");
        Matcher matcher = pattern.matcher("abc123def");

        // looking
        System.out.println("Looking At: " + matcher.lookingAt());
        System.out.println("Start: " + matcher.start());
        System.out.println("End: " + matcher.end());
        System.out.println("Group: " + matcher.group());

        matcher.reset();
        // match full
        System.out.println("\nMatch: ");
        System.out.println("Match: " + matcher.matches());

        matcher.reset();
        //find
        System.out.println("\nFind: ");
        System.out.println("Find: " + matcher.find());
        System.out.println("Start: " + matcher.start());
        System.out.println("End: " + matcher.end());
        System.out.println("Group: " + matcher.group());
        System.out.println("Find: " + matcher.find());
        System.out.println("Start: " + matcher.start());
        System.out.println("End: " + matcher.end());
        System.out.println("Group: " + matcher.group());
        System.out.println("Find: " + matcher.find());

        //group count
        System.out.println("Groups: " + matcher.groupCount());
    }

    static void regexExample2() {

        String REGEX = "а*д";
        String INPUT = "аадProgLangааадProgLangадProgLangдgdfgdfgdf";
        String REPLACE = "-";

        Pattern p = Pattern.compile(REGEX);

        // получение matcher объекта
        Matcher m = p.matcher(INPUT);
        StringBuffer sb = new StringBuffer();
        while(m.find()) {
            m.appendReplacement(sb, REPLACE);
        }
//        m.appendTail(sb);
        System.out.println(sb.toString());

    }

    static void regexExample3() {
        String EXAMPLE_TEST = "Text .";
        String pattern = "(\\w)(\\s+)([\\.,])";
        System.out.println(EXAMPLE_TEST.replaceAll(pattern, "$1$3"));
    }

    static void regexExample4() {
        String EXAMPLE_TEST = "Text .";
        String pattern = "(?<group1>\\w)(?<group2>\\s+)\\k<group1>";
        System.out.println(EXAMPLE_TEST.replaceAll(pattern, "${group1}${group3}"));
    }

    static void regexExample5() {
        String EXAMPLE_TEST = "Мой номер телефона 03\n\rМой номер телефона 03";
        String pattern = "(?m)(?=.*[0-9])(?=.*[а-яА-Я])\\AМой.+03\\z";
        System.out.println(EXAMPLE_TEST.matches(pattern));
    }

}
